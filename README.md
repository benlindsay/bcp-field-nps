# bcp-field-nps

Discrete Polymer Field Theory code that can simulate bare fixed or free nanospheres or nanorods in a diblock copolymer or homopolymer matrix.
